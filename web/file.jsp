<%@ page import="business.FileLogic" %>
<%@ page import="dto.File" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: cuiziang
  Date: 2018-12-08
  Time: 17:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>


<main role="main" class="container">

    <div class="row">
        <div class="col mt-6">
            <h2>File Table</h2>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <!-- https://www.w3schools.com/css/css_table.asp -->
            <form action="fileform" method="post">
                <div class="col-6">

                    <table class="table">
                        <tr>
                            <td><input type="text" name="searchText"/></td>
                            <td><input type="button" name="search" value="Search"/></td>
                        </tr>
                    </table>

                </div>

                <table class="table table-hover table-bordered" align="center">
                    <thead>
                    <tr>
                        <th class="delete"><input type="submit" name="deleteFileSubmit" value="Delete"/></th>
                        <th>Edit</th>
                        <th>File ID</th>
                        <th>File content</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        FileLogic logic = new FileLogic();
                        List<File> files = logic.getAllFiles();
                        long counter = 0;
                        for (File file : files) {
                    %>
                    <tr>
                        <td>
                            <input type="checkbox" name="deleteMark" value="<%=file.getId()%>"/>
                        </td>
                        <td class="edit" id="<%=counter++%>"><input class="update" type="button" name="edit"
                                                                    value="Edit"/>
                        </td>
                        <td class="code" id="<%=counter++%>"><%=file.getId()%>
                        </td>
                        <td class="code" id="<%=counter++%>"><%=file.getFile()%>
                        </td>
                    </tr>
                    </tbody>
                    <%
                        }
                    %>
                    <thead>
                    <tr>
                        <th><input class="delete" type="submit" name="deleteFileSubmit" value="Delete"/></th>
                        <th>Edit</th>
                        <th>File ID</th>
                        <th>File content</th>
                    </tr>
                    </thead>
                </table>
            </form>
            <a class="btn btn-primary" href="addfile" role="button">Upload File</a>
            <div style="text-align: center;">
                <pre><%=toStringMap(request.getParameterMap())%></pre>
            </div>
            <%!
                private String toStringMap(Map<String, String[]> m) {
                    StringBuilder builder = new StringBuilder();
                    for (String k : m.keySet()) {
                        builder.append("Key=").append(k)
                                .append(", ")
                                .append("Value/s=").append(Arrays.toString(m.get(k)))
                                .append(System.lineSeparator());
                    }
                    return builder.toString();
                }
            %>
        </div>
    </div>

</main>
<!-- /.container -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<%@include file="footer.jsp" %>
