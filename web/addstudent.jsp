<%--
  Created by IntelliJ IDEA.
  User: cuiziang
  Date: 2018-12-08
  Time: 10:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>

<%
    String errorMessage = request.getParameter("errorMessage");
%>
<main role="main" class="container">

    <div class="row">
        <div class="col mt-6">
            <h2>Add Student</h2>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div style="display: inline-block; text-align: left;">
                <form action="studentform" method="post">
                    <div class="form-group">
                        <label for="id">Student ID</label>
                        <input type="number" class="form-control" id="id" name="id">
                    </div>
                    <div class="form-group">
                        <label for="firstName">First Name</label>
                        <input type="text" class="form-control" id="firstName" name="firstName">
                    </div>
                    <div class="form-group">
                        <label for="lastName">Last Name</label>
                        <input type="text" class="form-control" id="lastName" name="lastName">
                    </div>
                    <button type="submit" name="addStudentSubmit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>


</main>
<!-- /.container -->

<%@include file="footer.jsp" %>