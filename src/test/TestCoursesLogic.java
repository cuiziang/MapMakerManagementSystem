package test;

import business.CourseLogic;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.jupiter.api.*;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * a demo of JUnit for running a test on DAO with Tomcat using TomcatJNDI
 *
 * @author Shahriar (Shawn) Emami
 * @date November 11, 2018
 * @see <a href="https://www.youtube.com/watch?v=N8uZnPR5QVw">JUnit tutorial(youtube)</a>
 * @see <a href="https://github.com/h-thurow/TomcatJNDI">TomcatJNDI for JUnit</a>
 */
public class TestCoursesLogic {

    private static TomcatJNDI tomcatJNDI;
    private CourseLogic logic;

    @BeforeAll
    public static void setUpClass() {
        tomcatJNDI = new TomcatJNDI();
        String webFolder = "web";
        if (!Files.exists(Paths.get(webFolder))) {
            webFolder = "WebContent";
        }
        tomcatJNDI.processContextXml(Paths.get(webFolder + "\\META-INF\\context.xml").toFile());
        tomcatJNDI.processWebXml(Paths.get(webFolder + "\\WEB-INF\\web.xml").toFile());
        tomcatJNDI.start();
    }

    @AfterAll
    public static void tearDownClass() {
        tomcatJNDI.tearDown();
    }

    @BeforeEach
    public void setup() {
        logic = new CourseLogic();
    }

    @Test
    public void testGetAllCourses() {
        assertEquals(24, logic.getAllCourses().size());
    }

    @AfterEach
    public void tearDown() {
        logic = null;
    }
}