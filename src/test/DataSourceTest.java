package test;

import dataaccess.DataSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class DataSourceTest {

    private Connection connection;

    @BeforeEach
    void setUp() {
        connection = new DataSource().createConnection();
    }

    @Test
    void createConnection() {
        assertNotNull(connection);
    }
}