package business;

import dataaccess.FileDAO;
import dataaccess.FileDAOImpl;
import dto.File;
import dto.factory.DTOFactoryCreator;
import dto.factory.Factory;

import java.io.InputStream;
import java.util.List;

/**
 * @author Min Jia
 */
public class FileLogic {
    private static final int FILE_ID_MAX_LENGTH = 45;
    private static final int FILE_NAME_MAX_LENGTH = 45;

    private FileDAO fileDAO = null;
    private Factory<File> factory = null;

    public FileLogic() {
        fileDAO = new FileDAOImpl();
        factory = DTOFactoryCreator.createFactory(File.class);
    }

    public List<File> getAllFiles() {
        return fileDAO.getAll();
    }

    public int addFile(InputStream stream) {
//        validateFile(stream);
//        cleanFile(stream);
        return fileDAO.add(stream);
    }

    public void deleteFiles(String[] ids) throws ValidationException {
        for (String id : ids) {
            deleteFile(id);
        }
    }

    public void deleteFile(String code) throws ValidationException {
        fileDAO.delete(code);
    }

//    private void cleanFile(InputStream stream) {
//        if (stream.getId() != null) {
//            stream.setId(stream.getId().trim());
//        }
//        if (file.getFile() != null) {
//            file.setFile(file.getFile());
//        }
//    }

//    private void validateFile(InputStream stream) throws ValidationException {
//        validateString(file.getId(), "File id", FILE_ID_MAX_LENGTH, false);
//        validateString(file.getFile().toString(), "File name", FILE_NAME_MAX_LENGTH, false);
//    }

    private void validateString(String value, String fieldName, int maxLength, boolean isNullAllowed) throws ValidationException {
        if (value == null && isNullAllowed) {
            // null permitted, nothing to validate
        } else if (value == null && !isNullAllowed) {
            throw new ValidationException(String.format("%s cannot be null", fieldName));
        } else if (value.isEmpty()) {
            throw new ValidationException(String.format("%s cannot be empty or only whitespace", fieldName));
        } else if (value.length() > maxLength) {
            throw new ValidationException(String.format("%s cannot exceed %d characters", fieldName, maxLength));
        }
    }

}