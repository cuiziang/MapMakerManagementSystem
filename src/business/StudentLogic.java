package business;


import dataaccess.DAOInterface;
import dataaccess.StudentDAOImpl;
import dto.Student;
import dto.factory.DTOFactoryCreator;
import dto.factory.Factory;

import java.util.List;

public class StudentLogic {

    private DAOInterface<Student> dao = null;
    private Factory<Student> factory = null;

    public StudentLogic() {
        dao = new StudentDAOImpl();
        factory = DTOFactoryCreator.createFactory(Student.class);
    }

    public List<Student> getAllStudents() {
        return dao.getAll();
    }

    public void add(Student student) {
        dao.add(student);
    }

    public void deleteStudents(String[] codes) {
        for (String code : codes) {
            deleteStudent(code);
        }
    }

    public void deleteStudent(String code) {
        dao.delete(code);
    }
}
