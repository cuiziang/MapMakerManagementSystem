package view;

import business.StudentLogic;
import dto.Student;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StudentForm extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StudentLogic studentLogic = new StudentLogic();

        if (req.getParameter("addStudentSubmit") != null) {
            String id = req.getParameter("id");
            String firstName = req.getParameter("firstName");
            String lastName = req.getParameter("lastName");
            Student student = new Student(id, firstName, lastName);
            studentLogic.add(student);
            resp.sendRedirect("student");
        } else if (req.getParameter("deleteStudentSubmit") != null) {
            studentLogic.deleteStudents(req.getParameterValues("deleteMark"));
            resp.sendRedirect("student");
        } else {
            resp.sendRedirect("student");
        }
    }
}
