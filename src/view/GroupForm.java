package view;

import business.GroupLogic;
import dto.Group;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GroupForm extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GroupLogic groupLogic = new GroupLogic();
        String d = req.getParameter("deleteGroupSubmit");
        if (req.getParameter("addGroupSubmit") != null) {
            String name = req.getParameter("groupName");
            Group group = new Group(name);
            groupLogic.addGroup(group);
            resp.sendRedirect("group");
        } else if (req.getParameter("deleteGroupSubmit") != null) {
            groupLogic.deleteGroup(req.getParameterValues("deleteMark"));
            resp.sendRedirect("group");
        } else {
            resp.sendRedirect("group");
        }
    }
}
