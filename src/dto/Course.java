package dto;


/**
 * transfer object for Course
 *
 * @author Shawn
 */
public class Course {

    public static final String COL_NAME = "name";
    public static final String COL_CODE = "course_num";

    private String name;
    private String code;

    public Course() {
    }

    public Course(String name, String code) {
        setName(name);
        setCode(code);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}