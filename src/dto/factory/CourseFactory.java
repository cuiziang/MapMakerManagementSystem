//package dto.factory;
//
//import dto.Course;
//import dto.builder.CourseBuilder;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.Map;
//
///**
// *
// *
// * @author Shariar
// */
//public class CourseFactory extends Factory< Course> {
//
//    @Override
//    public Course createFromResultSet(ResultSet rs) throws SQLException {
//        if (rs == null || !rs.next()) {
//            return null;
//        }
//        CourseBuilder builder = new CourseBuilder();
//        builder.setCode(rs.getString(Course.COL_CODE))
//                .setName(rs.getString(Course.COL_NAME));
//        return builder.get();
//    }
//
//    @Override
//    public Course createFromMap(Map< String, String[]> map) {
//        if (map == null || map.isEmpty()) {
//            return null;
//        }
//        CourseBuilder builder = new CourseBuilder();
//        builder.setCode(map.get(Course.COL_CODE)[0])
//                .setName(map.get(Course.COL_NAME)[0]);
//        return builder.get();
//    }
//}
