/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.factory;

import dto.Group;

/**
 * @author Min Jia
 */
public class GroupBuilder {

    private final Group group = new Group();

    public GroupBuilder setName(String name) {
        group.setName(name);
        return this;
    }


    public Group get() {
        return group;
    }

}
