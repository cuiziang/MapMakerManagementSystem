/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataaccess;

import dto.File;

import java.io.InputStream;
import java.util.List;

/**
 * @author Min Jia
 */
public interface FileDAO {
    int add(InputStream stream);

    File getById(String id);

    List<File> getAll();

    void update(String id, InputStream stream);

    void delete(String id);

    void deleteAll(String[] ids);
}