/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataaccess;

import dto.FileDetail;

import java.io.InputStream;
import java.util.List;

/**
 * @author Min Jia
 */
public interface FileDetailDAO extends DAOInterface<FileDetail> {
    void add(InputStream stream);

    FileDetail getById(int id);

    List<FileDetail> getAll();

    void update(int id, InputStream stream);

    void delete(int id);

    void deleteAll(int[] ids);

}