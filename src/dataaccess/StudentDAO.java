/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataaccess;

import dto.Student;

import java.util.List;

/**
 * @author
 */
public interface StudentDAO extends DAOInterface<Student> {
    List<Student> getAll();

    void add(Student student);

    void delete(String str);

    void deleteAll(String[] str);

    Student getById(String id);

}