package dataaccess;

import dto.Course;
import dto.factory.DTOFactoryCreator;
import dto.factory.Factory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Shawn
 */
public class CourseDAOImpl implements DAOInterface<Course> {

    private static final String GET_ALL_COURSES = "SELECT course_num, name FROM Courses ORDER BY course_num";
    private static final String INSERT_COURSES = "INSERT INTO Courses (course_num, name) VALUES(?, ?)";
    private static final String DELETE_COURSE = "DELETE FROM Courses WHERE course_num = ?";
    private static final String DELETE_COURSES = "DELETE FROM Courses WHERE (course_num) IN ";
    private static final String UPDATE_COURSES = "UPDATE Courses SET name = ? WHERE course_num = ?";
    private static final String GET_BY_CODE_COURSES = "SELECT course_num, name FROM Courses WHERE name = ?";

    private final Factory<Course> factory = DTOFactoryCreator.createFactory(Course.class);

    @Override
    public List<Course> getAll() {
        List<Course> courses = Collections.emptyList();
        Course course;
        try (Connection con = new DataSource().createConnection();
             PreparedStatement pstmt = con.prepareStatement(GET_ALL_COURSES);
             ResultSet rs = pstmt.executeQuery();) {
            //courses = factory.createListFromResultSet(rs);
            courses = new ArrayList<>(100);
            while (rs.next()) {
                course = new Course();
                course.setCode(rs.getString(Course.COL_CODE));
                course.setName(rs.getString(Course.COL_NAME));
                courses.add(course);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return courses;
    }

    @Override
    public void add(Course course) {
        try (Connection con = new DataSource().createConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_COURSES);) {
            pstmt.setString(1, course.getCode());
            pstmt.setString(2, course.getName());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(String code) {
        try (Connection con = new DataSource().createConnection();
             PreparedStatement pstmt = con.prepareStatement(DELETE_COURSE);) {
            pstmt.setString(1, code);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteAll(String[] str) {
        StringBuilder query = new StringBuilder(DELETE_COURSES);
        query.append("(");
        String delimiter = "";
        for (int i = 0; i < str.length; i++) {
            query.append(delimiter).append("(?)");
            delimiter = ",";
        }
        query.append(")");
        try (Connection con = new DataSource().createConnection();
             PreparedStatement pstmt = con.prepareStatement(query.toString());) {
            for (int i = 0; i < str.length; i++) {
                pstmt.setString(i + 1, str[i]);
            }
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Course getById(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
